/* main.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GLib;
using GXml;
using Gresg;

public class Gresg.App : Application {
  private static bool version = false;
  private static string? filename = null;
  private static string? res_prefix = null;
  private static Gresg.Resources _resources;
  private const GLib.OptionEntry[] options = {
    // --version
    { "version", 0, 0, OptionArg.NONE, ref version, "Display version number", null },

    // --file FIlENAME || -o FILENAME
    { "file", 'o', 0, OptionArg.FILENAME, ref filename, "Output file", "FILE" },

    // --res-prefix PREFIX || -p PREFIX
    { "prefix", 'p', 0, OptionArg.STRING, ref res_prefix, "Default Prefix for resources", "PREFIX" },
    // list terminator
    { null }
  };

  static int main (string[] args) {
    var opt_context = new OptionContext ("XML GResources generator");
    try {
      opt_context.set_help_enabled (true);
      opt_context.add_main_entries (options, null);
      opt_context.set_ignore_unknown_options (false);
      opt_context.parse (ref args);
    } catch (OptionError e) {
      stdout.printf ("Error: %s\n", e.message);
      stdout.printf ("Run '%s --help' to see a full list of available command line options.\n", args[0]);
      return 0;
    }
    if (version) {
      stdout.printf (Config.PACKAGE_NAME+" "+Config.VERSION+"\n");
      return 0;
    }
    if (args.length == 1) {
      stdout.printf ("\nError: You shoudl provide at least one file to add into resource file\n");
      stdout.printf (opt_context.get_help (true, null));
      return 1;
    }
    _resources = new Gresg.Resources ();
    for (int i = 1; i < args.length; i++) {
      string str = args[i];
      try {
        var file = GLib.File.new_for_path (str);
        if (!file.query_exists ()) {
          stdout.printf ("File not found: "+file.get_path ()+"\n");
          return 1;
        }
        _resources.add_file (file.get_basename ());
      }
      catch (GLib.Error e) {
        stdout.printf ("Error: "+e.message+"\n");
        return 1;
      }
    }
    if (res_prefix != null)
      _resources.resource.prefix = res_prefix;
    try {
      if (filename == null) filename = "resources.xml";
      var rf = GLib.File.new_for_path (filename);
        _resources.write_file (rf);
    } catch (GLib.Error e) {
      stdout.printf ("Error: "+e.message+"\n");
    }
    return 0;
  }
}

